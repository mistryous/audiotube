# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the audiotube package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: audiotube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 00:47+0000\n"
"PO-Revision-Date: 2023-04-27 06:50+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.2.2\n"

#: asyncytmusic.cpp:65
#, kde-format
msgid ""
"Running with untested version of ytmusicapi %1. If you experience errors, "
"please report them to your distribution."
msgstr ""
"Poganjanje netestirane različice ytmusicapi %1. Če ste izkusili napako, jo "
"poročajte vaši distribuciji."

#: contents/ui/AlbumPage.qml:75 contents/ui/LibraryPage.qml:37
#: contents/ui/LibraryPage.qml:63 contents/ui/LibraryPage.qml:84
#: contents/ui/LibraryPage.qml:222 contents/ui/LibraryPage.qml:248
#: contents/ui/LibraryPage.qml:269 contents/ui/LocalPlaylistPage.qml:70
#: contents/ui/LocalPlaylistPage.qml:92 contents/ui/PlaybackHistory.qml:67
#: contents/ui/PlaybackHistory.qml:89 contents/ui/PlaylistPage.qml:69
#: contents/ui/PlaylistPage.qml:84
#, kde-format
msgid "Play"
msgstr "Predvajaj"

#: contents/ui/AlbumPage.qml:82 contents/ui/ArtistPage.qml:87
#: contents/ui/LibraryPage.qml:44 contents/ui/LibraryPage.qml:69
#: contents/ui/LibraryPage.qml:92 contents/ui/LibraryPage.qml:229
#: contents/ui/LibraryPage.qml:254 contents/ui/LibraryPage.qml:278
#: contents/ui/LocalPlaylistPage.qml:65 contents/ui/LocalPlaylistPage.qml:99
#: contents/ui/PlaybackHistory.qml:55 contents/ui/PlaybackHistory.qml:102
#: contents/ui/PlaylistPage.qml:62 contents/ui/PlaylistPage.qml:91
#, kde-format
msgid "Shuffle"
msgstr "Premešaj"

#: contents/ui/AlbumPage.qml:90 contents/ui/LibraryPage.qml:74
#: contents/ui/LibraryPage.qml:100 contents/ui/LibraryPage.qml:259
#: contents/ui/LibraryPage.qml:286 contents/ui/LocalPlaylistPage.qml:105
#: contents/ui/PlaybackHistory.qml:114 contents/ui/PlaylistPage.qml:99
#, kde-format
msgid "Append to queue"
msgstr "Dodaj v v čakalno vrsto"

#: contents/ui/AlbumPage.qml:95 contents/ui/ArtistPage.qml:95
#: contents/ui/PlaylistPage.qml:104
#, kde-format
msgid "Open in Browser"
msgstr "Odpri v brskalniku"

#: contents/ui/AlbumPage.qml:100 contents/ui/ArtistPage.qml:100
#: contents/ui/PlaylistPage.qml:109
#, kde-format
msgid "Share"
msgstr "Deli"

#: contents/ui/AlbumPage.qml:110
#, kde-format
msgid "Album • %1"
msgstr "Album • %1"

#: contents/ui/AlbumPage.qml:162 contents/ui/ArtistPage.qml:199
#: contents/ui/LocalPlaylistPage.qml:170 contents/ui/PlaybackHistory.qml:194
#: contents/ui/PlaylistPage.qml:179 contents/ui/SearchPage.qml:144
#, kde-format
msgid "More"
msgstr "Več"

#: contents/ui/ArtistPage.qml:81
#, kde-format
msgid "Radio"
msgstr "Radio"

#: contents/ui/ArtistPage.qml:109
#, kde-format
msgid "Artist"
msgstr "Avtor"

#: contents/ui/ArtistPage.qml:131 contents/ui/SearchPage.qml:72
#, kde-format
msgid ""
"Video playback is not supported yet. Do you want to play only the audio of "
"\"%1\"?"
msgstr "Video playback še ni podprt. Ali želite predvajati samo zvok \"%1\"?"

#: contents/ui/ArtistPage.qml:148 contents/ui/SearchPage.qml:26
#, kde-format
msgid "Albums"
msgstr "Albumi"

#: contents/ui/ArtistPage.qml:150
#, kde-format
msgid "Singles"
msgstr "Singli"

#: contents/ui/ArtistPage.qml:152 contents/ui/SearchPage.qml:32
#, kde-format
msgid "Songs"
msgstr "Glasba"

#: contents/ui/ArtistPage.qml:154 contents/ui/SearchPage.qml:34
#, kde-format
msgid "Videos"
msgstr "Videposnetki"

#: contents/ui/ConfirmationMessage.qml:16
#, kde-format
msgid "OK"
msgstr "V redu"

#: contents/ui/ConfirmationMessage.qml:21
#, kde-format
msgid "Cancel"
msgstr "Prekliči"

#: contents/ui/dialogs/AddPlaylistDialog.qml:17
#, kde-format
msgid "Add playlist"
msgstr "Dodaj seznam predvajanja"

#: contents/ui/dialogs/AddPlaylistDialog.qml:23
#: contents/ui/dialogs/RenamePlaylistDialog.qml:24
#, kde-format
msgid "Playlist Title"
msgstr "Naslov seznama predvajanja"

#: contents/ui/dialogs/AddPlaylistDialog.qml:28
#: contents/ui/dialogs/RenamePlaylistDialog.qml:30
#, kde-format
msgid "Playlist Description"
msgstr "Opis seznama predvajanja"

#: contents/ui/dialogs/PlaylistDialog.qml:20
#, kde-format
msgid "Add Song to Playlist"
msgstr "Dodaj pesem v seznam predvajanja"

#: contents/ui/dialogs/PlaylistDialog.qml:29
#: contents/ui/LocalPlaylistsPage.qml:90
#, kde-format
msgid "New Playlist"
msgstr "Novi seznam predvajanja"

#: contents/ui/dialogs/RenamePlaylistDialog.qml:18
#, kde-format
msgid "Rename playlist"
msgstr "Preimenuj seznam predvajanja"

#: contents/ui/LibraryPage.qml:30 contents/ui/LibraryPage.qml:123
#: contents/ui/NavigationBar.qml:48 contents/ui/NavigationBar.qml:54
#: contents/ui/Sidebar.qml:131 contents/ui/Sidebar.qml:138
#, kde-format
msgid "Favourites"
msgstr "Priljubljene"

#: contents/ui/LibraryPage.qml:119 contents/ui/LibraryPage.qml:306
#: contents/ui/LibraryPage.qml:403
#, kde-format
msgid "Show All"
msgstr "Prikaži vse"

#: contents/ui/LibraryPage.qml:147
#, kde-format
msgid "No Favourites Yet"
msgstr "Še ni nobenih priljubljenih"

#: contents/ui/LibraryPage.qml:215
#, kde-format
msgid "Most played"
msgstr "Največkrat predvajane"

#: contents/ui/LibraryPage.qml:310 contents/ui/NavigationBar.qml:61
#: contents/ui/NavigationBar.qml:67 contents/ui/Sidebar.qml:150
#: contents/ui/Sidebar.qml:157
#, kde-format
msgid "Played Songs"
msgstr "Predvajane pesmi"

#: contents/ui/LibraryPage.qml:334
#, kde-format
msgid "No Songs Played Yet"
msgstr "Še ni nobene predvajane pesmi"

#: contents/ui/LibraryPage.qml:391 contents/ui/LocalPlaylistsPage.qml:18
#: contents/ui/LocalPlaylistsPage.qml:78 contents/ui/NavigationBar.qml:75
#: contents/ui/SearchPage.qml:30 contents/ui/Sidebar.qml:167
#, kde-format
msgid "Playlists"
msgstr "Seznami predvajanja"

#: contents/ui/LibraryPage.qml:430
#, kde-format
msgid "No Playlists Yet"
msgstr "Še ni nobenega seznama predvajanja"

#: contents/ui/LibraryPage.qml:450 contents/ui/LibraryPage.qml:472
#: contents/ui/LocalPlaylistsPage.qml:33 contents/ui/LocalPlaylistsPage.qml:55
#, kde-format
msgid "Rename"
msgstr "Preimenuj"

#: contents/ui/LibraryPage.qml:459 contents/ui/LibraryPage.qml:480
#: contents/ui/LocalPlaylistsPage.qml:42 contents/ui/LocalPlaylistsPage.qml:63
#, kde-format
msgid "Delete"
msgstr "Zbriši"

#: contents/ui/LocalPlaylistPage.qml:21
#, kde-format
msgid "Remove from Playlist"
msgstr "Odstrani iz seznama predvajanja"

#: contents/ui/LocalPlaylistPage.qml:79
#, kde-format
msgid "This playlist is still empty"
msgstr "Ta seznam predvajanja je še prazen"

#: contents/ui/main.qml:110 main.cpp:61
#, kde-format
msgid "AudioTube"
msgstr "AudioTube"

#: contents/ui/main.qml:260
#, kde-format
msgid "No song playing"
msgstr "Nobene pesmi se ne predvaja"

#: contents/ui/MaximizedPlayerPage.qml:96
#, kde-format
msgid "Close Maximized Player"
msgstr "Zapri maksimiziran predvajalnik"

#: contents/ui/MaximizedPlayerPage.qml:200
#: contents/ui/MinimizedPlayerControls.qml:94
#, kde-format
msgid "No media playing"
msgstr "Niče se ne predvaja"

#: contents/ui/MaximizedPlayerPage.qml:423
#, kde-format
msgid "Remove from Favourites"
msgstr "Odstrani iz priljubljenih"

#: contents/ui/MaximizedPlayerPage.qml:423
#, kde-format
msgid "Add to Favourites"
msgstr "Dodaj med priljubljene"

#: contents/ui/MaximizedPlayerPage.qml:448
#, kde-format
msgid "Open Volume Drawer"
msgstr "Odpri predal glasnosti"

#: contents/ui/MaximizedPlayerPage.qml:556
#, kde-format
msgid "Unmute Audio"
msgstr "Povrni zvok"

#: contents/ui/MaximizedPlayerPage.qml:556
#, kde-format
msgid "Mute Audio"
msgstr "Utišaj"

#: contents/ui/MaximizedPlayerPage.qml:599
#, kde-format
msgid "%1%"
msgstr "%1%"

#: contents/ui/MaximizedPlayerPage.qml:622
#, kde-format
msgid "Hide Lyrics"
msgstr "Skrij besedila"

#: contents/ui/MaximizedPlayerPage.qml:622
#, kde-format
msgid "Show Lyrics"
msgstr "Prikazuj besedila"

#: contents/ui/MaximizedPlayerPage.qml:642 contents/ui/SongMenu.qml:183
#: contents/ui/SongMenu.qml:269
#, kde-format
msgid "Share Song"
msgstr "Deli pesem"

#: contents/ui/MaximizedPlayerPage.qml:688
#, kde-format
msgid "Add to a local playlist"
msgstr "Dodaj v lokalni seznam predvajanja"

#: contents/ui/MaximizedPlayerPage.qml:725
#, kde-format
msgid "Hide Queue"
msgstr "Skrij čakalno vrsto"

#: contents/ui/MaximizedPlayerPage.qml:725
#, kde-format
msgid "Show Queue"
msgstr "Prikazuj čakalno vrsto"

#: contents/ui/MaximizedPlayerPage.qml:922
#: contents/ui/MaximizedPlayerPage.qml:1154
#, kde-format
msgid "Remove Track"
msgstr "Odstrani sled"

#: contents/ui/MaximizedPlayerPage.qml:959
#, kde-format
msgid "Clear Queue"
msgstr "Počisti čakalno vrsto"

#: contents/ui/MaximizedPlayerPage.qml:984
#, kde-format
msgid "Shuffle Queue"
msgstr "Premešaj čakalno vrsto"

#: contents/ui/MaximizedPlayerPage.qml:1014
#, kde-format
msgid "Upcoming Songs"
msgstr "Prihajajoče pesmi"

#: contents/ui/NavigationBar.qml:24 contents/ui/Sidebar.qml:100
#, kde-format
msgid "Library"
msgstr "Knjižnica"

#: contents/ui/NavigationBar.qml:36 contents/ui/Sidebar.qml:116
#, kde-format
msgid "Search"
msgstr "Išči"

#: contents/ui/PlaybackHistory.qml:17
#, kde-format
msgid "Unknown list of songs"
msgstr "Neznan seznam pesmi"

#: contents/ui/PlaybackHistory.qml:135
#, kde-format
msgid "No songs here yet"
msgstr "Tu še ni nobene pesmi"

#: contents/ui/PlaylistPage.qml:120
#, kde-format
msgid "Playlist"
msgstr "Seznam predvajanja"

#: contents/ui/SearchHistoryPage.qml:86 contents/ui/SearchWithDropdown.qml:396
#, kde-format
msgid "remove from search history"
msgstr "odstrani iz zgodovine iskanj"

#: contents/ui/SearchPage.qml:16
#, kde-format
msgid "Previous Searches:"
msgstr "Prejšnja iskanja:"

#: contents/ui/SearchPage.qml:28
#, kde-format
msgid "Artists"
msgstr "Avtorji"

#: contents/ui/SearchPage.qml:36
#, kde-format
msgid "Top Results"
msgstr "Najboljši rezultati"

#: contents/ui/SearchPage.qml:39
#, kde-format
msgid "Unknown"
msgstr "Neznano"

#: contents/ui/SearchWithDropdown.qml:343
#, kde-format
msgid "No matching previous searches"
msgstr "Ni zadetkov iz prejšnjih iskanj"

#: contents/ui/ShareMenu.qml:39
#, kde-format
msgid "Share to"
msgstr "Deli z"

#: contents/ui/ShareMenu.qml:49 contents/ui/ShareMenu.qml:99
#, kde-format
msgid "Copy Link"
msgstr "Kopiraj povezavo"

#: contents/ui/ShareMenu.qml:54 contents/ui/ShareMenu.qml:104
#, kde-format
msgid "Link copied to clipboard"
msgstr "Povezava skopirana na odložišče"

#: contents/ui/Sidebar.qml:189
#, kde-format
msgid "About"
msgstr "O programu"

#: contents/ui/Sidebar.qml:203
#, kde-format
msgid "Collapse Sidebar"
msgstr "Strni stransko vrstico"

#: contents/ui/SongMenu.qml:107 contents/ui/SongMenu.qml:205
#, kde-format
msgid "Play Next"
msgstr "Predvajaj naslednjo"

#: contents/ui/SongMenu.qml:116 contents/ui/SongMenu.qml:211
#, kde-format
msgid "Add to queue"
msgstr "Dodaj v v čakalno vrsto"

#: contents/ui/SongMenu.qml:126 contents/ui/SongMenu.qml:221
#, kde-format
msgid "Remove Favourite"
msgstr "Odstrani priljubljenega"

#: contents/ui/SongMenu.qml:126 contents/ui/SongMenu.qml:221
#, kde-format
msgid "Add Favourite"
msgstr "Dodaj priljubljenega"

#: contents/ui/SongMenu.qml:143 contents/ui/SongMenu.qml:235
#, kde-format
msgid "Remove from History"
msgstr "Odstrani iz zgodovine"

#: contents/ui/SongMenu.qml:156 contents/ui/SongMenu.qml:244
#, kde-format
msgid "Add to playlist"
msgstr "Dodaj v seznam predvajanja"

#: main.cpp:63
#, kde-format
msgid "YouTube Music Player"
msgstr "Predvajalnik glasbe YouTube"

#: main.cpp:65
#, kde-format
msgid "© 2021-2023 Jonah Brüchert, 2021-2023 KDE Community"
msgstr "© 2021-2023 Jonah Brüchert, 2021-2023 Skupnost KDE"

#: main.cpp:66
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"

#: main.cpp:66
#, kde-format
msgid "Maintainer"
msgstr "Vzdrževalec"

#: main.cpp:67
#, kde-format
msgid "Mathis Brüchert"
msgstr "Mathis Brüchert"

#: main.cpp:67
#, kde-format
msgid "Designer"
msgstr "Oblikovalec"

#: main.cpp:68
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran"

#: main.cpp:68
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net"

#: main.cpp:73
#, kde-format
msgid "Unofficial API for YouTube Music"
msgstr "Neuradni programski vmesnik za Youtube Music"

#~ msgid "Clear Playlist"
#~ msgstr "Očisti seznam predvajanja"

#~ msgid "played Songs"
#~ msgstr "predvajane pesmi"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Now Playing"
#~ msgstr "Pravkar predvajano"

#~ msgid "Back"
#~ msgstr "Nazaj"

#~ msgid "Play next"
#~ msgstr "Predvajaj naslednjo"

#~ msgid "Clear"
#~ msgstr "Očisti"

#~ msgid "Pause"
#~ msgstr "Premor"

#~ msgid "Expand"
#~ msgstr "Razširi"

#~ msgid "Play only Audio"
#~ msgstr "Predvajaj samo zvok"
